import React, { Component } from "react";
//plug-in app's state=> React Redux connects react & redux;
//Container component (smart component)
import { connect } from "react-redux";

import { selectBook } from "../actions/index";
//we are binding action creator to this particular component;
//here we take that value from action creator and make sure it flows through reducers
import { bindActionCreators } from "redux";

class BookList extends Component {

    renderList() {
        return this.props.books.map(book => {
            return <li
                //when user selects, clicks particular book          
                onClick={() => { this.props.selectBook(book) }}
                //selectBook is called
                //when it's called; its result is dispatched to reducers through
                //function mapDispatchToProps() which returns redux method bindActionCreators()
                //bindActionCreators actualy does the work-takes action returned from selectBook
                //and dispatches it to all reducers in app
                key={book.title}
                className="list-group-item">{book.title}</li>
        });
    }


    render() {
        return (
            <ul className="list-group col-sm-4">
                {this.renderList()}
            </ul>
        )
    }
}


//take our app's state (whole state!) as argument:
//whatever gets returned will show as props inside of BookList;
//object is returned;will be assigned as props to component BookList.

function mapStateToProps(state) {
    return {
        books: state.books
    }
}


//Anything we return from this function will end up as props in BookList container component;
//it will be : this.props.selectBook //key is important!!!!!
function mapDispatchToProps(dispatch) {
    // 1 .selectBook is action creator we imported-it returns action-object containing value
    //that is going to update piece of our app's state({type:"Piece_of_state", keyName:valueOfPieceOfState})

    // 2. whenever selectBook is called-BookList will call it (and it is called on some user event, user selects book),
    //  the result should be passed to all of our reducers;

    // 3. for that we use bindActionCreators() from redux,(gets returned from function mapDispatchToProps)

    // bindActionCreators() takes 2 arguments(value from action creator and dispatch function);
    //it makes sure that action from selectBook goes to dispatch() function;

    // 4. dispatch receives actions, and spits them to different reducers in our app;
    return bindActionCreators({ selectBook: selectBook }, dispatch);
}


//use connect function, return container;
//connect function takes functions and component and produces container;
//container is aware of piece of state of app and is also aware of new dispatch method, selectBook;
export default connect(mapStateToProps, mapDispatchToProps)(BookList);