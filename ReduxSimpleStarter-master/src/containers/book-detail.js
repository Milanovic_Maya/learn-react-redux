import React, { Component } from "react";
import { connect } from "react-redux";


class BookDetail extends Component {
    render() {

        if (!this.props.book) {
            return <div>Select a book to get started.</div>
        }

        return (
            <div>
                <h3>Details for:</h3>
                <div>Title: {this.props.book.title}</div>
                <div>Pages: {this.props.book.pages}</div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    // state here is global app state
    //has key activeBook
    //produced by Reducer ActiveBook
    return {
        //our component gets props with property book
        book: state.activeBook
    }
}
//our component is now container
export default connect(mapStateToProps)(BookDetail);