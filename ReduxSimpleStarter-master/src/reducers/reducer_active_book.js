//all reducers get two arguments


//1. current state-that this reducer manages
//2.action

//reducers are called only when action occurs; 

//reducers ARE PURE FUNCTIONS!!!!

export default function (state = null, action) {
    //state is not app'state, it's piece of state
    //  that this reducer is responsible for

    //same state that this reducer produced earlier is flowing back
    //to same reducer-whenever action occurs

    switch (action.type) {
        case "BOOK_SELECTED":
            return action.payload;
    }


    //initial case is when none book is selected;
    //all of state is assembled by reducers
    //redux will throw some initial actions

    //redux doesn't allow to return undefined
    //we need to default value of state to null (ES6 syntax)

    //base case-when we don't care about current action
    return state;


}