import { combineReducers } from 'redux';
import BooksReducer from "./reducer_books";
import ActiveBook from "./reducer_active_book";


const rootReducer = combineReducers({
  //these keys will be properties to global app state
  //each reducer produces piece of app state
  books: BooksReducer,
  activeBook: ActiveBook
});

export default rootReducer;
//to wire reducers!!!
//connect() method from react-redux in container component