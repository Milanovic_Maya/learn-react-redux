//all the action creators will be here

export function selectBook(book) {
    //selectBook is an action creator
    //needs to return action
    //action is an object with type property
    //type value must be written this way
    //sometimes contains payload property(can be named as we like it)

    return {
        type: "BOOK_SELECTED",
        payload: book
    }

}

//we need to wire up to redux this action creator;

//we will bind this action creator selectBook to component BookList;